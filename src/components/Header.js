const Header = (props) => {

    return (<>
        <div className="add-to-cart">
            <img src="https://www.nicepng.com/png/full/261-2611207_cart-add-to-cart-icon-png.png" alt="" /><sup className="shop_count">{props.cartData.length}</sup>
        </div>
    </>)
}

export default Header