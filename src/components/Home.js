const Home = (props) => {
    return (<>
       

        <div className="container cart-container">
            <div className="item-wrapper">
                <div className="image-wrapper item">
                    <img src="https://i.gadgets360cdn.com/products/large/redmi-note-12-5g-pro-plus-db-gadgets360-800x600-1673019783.jpg?downsize=*:180" alt="" />
                </div>
                <div className="product-name-wrapper"><span>Phone</span> <br />
                </div>
                <div className="text-wrapper item">
                    <span className="price">Price $1000</span>
                </div>
                <div className="button-wrapper item">
                    <button onClick={() => props.addToCartHandler({ price: 1000, name: 'phone' })}>Add to cart</button>
                </div>
            </div>
        </div>
    </>)
}



export default Home;