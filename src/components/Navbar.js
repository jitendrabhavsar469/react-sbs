import { Link } from "react-router-dom"

const Navbar = ()=>{

    return(<>
    <ul>

    <li><Link to='/about'>About</Link></li>
    <li><Link to='/login'>Login</Link></li>
    <li><Link to='/user'>Users</Link></li>
    <li><Link to='/'>Home</Link></li>
    <li><Link to='/api'>API</Link></li>
    </ul>
    </>)
}

export default Navbar;