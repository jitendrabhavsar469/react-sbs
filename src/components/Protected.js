import { useEffect } from "react";
import { useNavigate } from "react-router"

const Protected = (props) => {
    const navigate = useNavigate();
    const { Component } = props
    useEffect(()=>{
        let login = false;
        if(!login){
            navigate('/login')
        }
    })

    return (
        <Component />
    )
}

export default Protected