import "./App.css";
import HomeContainer from '../src/containers/HomeContainer'
import HeaderContainer from "./containers/HeaderContainer";
function App() {
  return (<>
    <HeaderContainer />
    <HomeContainer />
  </>
  );
}
export default App;
