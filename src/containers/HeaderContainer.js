import { connect } from "react-redux";
import Header from "../components/Header";

const mapDispatchToProps = dispatch => ({
    // addToCartHandler: data => dispatch(addToCart(data))
});


const mapStateToProps = state => ({
    cartData:state.cartItems
})
export default connect(mapStateToProps, mapDispatchToProps)(Header);